# wikimenu

## What is wikimenu

Wikimenu allows wiki-style menu coding to GTK/GNOME applications.  
  
GTK's menu coding needs Glade and handle boring xml, or many similar codes.  
With Wikimenu, you can instantly make it with wiki-style text format.  