#
# Copyright (C) 2019 Yukihiro Nakai, All Rights Reserved.
#

import sys
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gio, Gtk

class App:
  def __init__(self):
    self.app = Gtk.Application.new('com.sample.gnome', 0)
    self.app.connect('startup', self.on_app_startup)
    self.app.connect('activate', self.on_app_activate)
    self.app.connect('shutdown', self.on_app_shutdown)

  def run(self, argv):
    self.app.run(argv)

  def on_reload_click(self, button, app):
    menu = App.load_wikimenu('menubar.txt')
    app.set_menubar(menu)

  def on_app_startup(self, app):
    self.window = Gtk.ApplicationWindow.new(app)
    self.window.set_default_size(400, 500)
    self.window.set_title('Wikimenu')

    self.button = Gtk.Button.new_with_label('Reload')
    self.button.connect('clicked', self.on_reload_click, app)
    self.window.add(self.button)
    self.window.show_all()

    app.add_window(self.window)

  def load_wikimenu(filename):
    """
    WikiMenu

    Parameters
    ----------
    filename:
      text file written in wikimenu format

    Returns
    -------
      an instance of Gio.Menu

    """

    menu = Gio.Menu()
    wikimenu = filter(lambda w: w[0] != '#', open(filename).readlines())
    wikimenu = list(map(lambda w: w[-1] == '\n' and w[:-1] or w, wikimenu))
    indentlevel = -1
    indent = '  '

    level = list(map(lambda i:(len(i)-len(i.lstrip(indent))) / len(indent), wikimenu))
    label = list(map(lambda i: i.lstrip(indent), wikimenu))
    #funclabel = map(lambda i: string.lower(string.replace(i, ' ', '_')), label)
    menu_hier = list(map(lambda w: None, wikimenu))
    menu_hier[0] = menu
    nextlevel = level[1:] + [1]
    for i in range(len(level)):
      if level[i] == 0 or nextlevel[i] == level[i] + 1:
        submenu = Gio.Menu()
        menu_hier[int(level[i])].append_submenu(label[i], submenu)

        menu_hier[int(level[i])+1] = submenu
        #globals().__setitem__(funclabel[i].lower() + '_menuitem', menuitem)
      elif label[i][:4] == '<sep':
        #menu_hier[level[i]].add(gtk.SeparatorMenuItem())
        pass
      else:
        menuitem = Gio.MenuItem.new(label[i], None)
        menu_hier[int(level[i])].append_item(menuitem)
#        try:
#          menuitem.connect('activate', eval('obj.' + funcname % funclabel[i].lower()))
#        except:
#          print "WARN (wikimenu): " + funcname % funclabel[i].lower() + " is not defined"
#          menuitem.connect('activate', lambda w: gtk.main_quit ())

    return menu

  def on_app_activate(self, app):
    self.window.show_all()
    menu = App.load_wikimenu('menubar.txt')
    app.set_menubar(menu)

  def on_app_shutdown(self, app):
    pass

if __name__ == '__main__':
  app = App()
  app.run(sys.argv)
